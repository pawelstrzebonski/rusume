# Rusume

A [JSON Resume](https://jsonresume.org/) tool, written in Rust.

## Features

* Validate JSON Resume against [official standard and schema](https://github.com/jsonresume/resume-schema)
* Generate Markdown (text) resumes
* Generate HTML resumes
* Generate resumes using built-in default templates or user-supplied templates

## Installation/Usage

You can build this project using `cargo build --release`.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.

This program is a CLI application. The arguments are:
* `--resume`: use to specify path to user's JSON resume file (will default to `resume.json` if not specified)
* `--verify`: if provided, will verify JSON resume against schema
* `--output`/`-o`: use to specify filepath root (excluding extension) of output files (will default to `resume` if not specified)
* `--md`: if provided, will generate Markdown resume
* `--html`: if provided, will generate HTML resume
* `--mdtemplate`: use to specify filepath of custom Markdown template file
* `--htmltemplate`: use to specify filepath of custom HTML template file

## Third-Party Resources

The `external/` directory contains third-party files.

The `external/jsonresume/resume-schema/` directory contains JSON files from [https://github.com/jsonresume/resume-schema](https://github.com/jsonresume/resume-schema) under an MIT license.
