use jsonschema::JSONSchema;
use std::fs;
use std::io::Write;
use tinytemplate::TinyTemplate;

pub fn main() {
    // Parse CLI arguments
    let mut pargs = pico_args::Arguments::from_env();
    // Whether to verify schema
    let verify = pargs.contains("--verify");
    // Whether to run MD template
    let md = pargs.contains("--md");
    // Optional custom MD template
    let md_template_filepath = pargs
        .value_from_str("--mdtemplate")
        .unwrap_or_else(|_| "".to_string());
    // Whether to run HTML template
    let html = pargs.contains("--html");
    // Optional custom HTML template
    let html_template_filepath = pargs
        .value_from_str("--htmltemplate")
        .unwrap_or_else(|_| "".to_string());
    // Filepath of JSON resume, default to 'resume.json'
    let json_resume_path = pargs
        .value_from_str("--resume")
        .unwrap_or_else(|_| "resume.json".to_string());
    // Filepath of output, default to 'resume.[EXTENSION]'
    let output_path_root = pargs
        .value_from_str(["-o", "--output"])
        .unwrap_or_else(|_| "resume".to_string());

    // Read resume file and parse JSON
    let json_resume = serde_json::from_str(&fs::read_to_string(json_resume_path).unwrap()).unwrap();
    if verify {
        //Load schema
        let schema = serde_json::from_str(include_str!(
            "../external/jsonresume/resume-schema/schema.json"
        ))
        .unwrap();
        // Compile schema
        let compiled = JSONSchema::compile(&schema).expect("A valid schema");
        // Validate resume against schema
        let result = compiled.validate(&json_resume);
        // If there are any validation errors, print them
        if let Err(errors) = result {
            for error in errors {
                println!("Validation error: {}", error);
                println!("Instance path: {}", error.instance_path);
            }
        } else {
            // If no errors, state so
            println!("JSON resume validated. No errors.");
        }
    }
    // Create templater
    let mut tt = TinyTemplate::new();
    // Load templates
    let template = if html_template_filepath.is_empty() {
        include_str!("../templates/resume.html.tmpl").to_string()
    } else {
        println!("Loading custom HTML template");
        fs::read_to_string(html_template_filepath).unwrap()
    };
    // Add to templater
    tt.add_template("html", &template).unwrap();
    // Load template
    let template = if md_template_filepath.is_empty() {
        include_str!("../templates/resume.md.tmpl").to_string()
    } else {
        println!("Loading custom MD template");
        fs::read_to_string(md_template_filepath).unwrap()
    };
    // Add to templater
    tt.add_template("md", &template).unwrap();
    // Run if HTML output is desired
    if html {
        // Render template
        let rendered = tt.render("html", &json_resume).unwrap();
        // Write to file
        let mut output = fs::File::create(output_path_root.clone() + ".html").unwrap();
        write!(output, "{}", rendered).unwrap();
    }
    // Run if MD output is desired
    if md {
        // Render template
        let rendered = tt.render("md", &json_resume).unwrap();
        // Write to file
        let mut output = fs::File::create(output_path_root + ".md").unwrap();
        write!(output, "{}", rendered).unwrap();
    }
}
