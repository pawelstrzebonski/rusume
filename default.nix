{ pkgs ? import <nixpkgs> {} }:
with pkgs;

rustPlatform.buildRustPackage rec {
	name = "rusume";
	cargoHash = "sha256-PvyYvf6kSDy6JR1P5f1ijVaAYDxuZLoBP7vLc3QcskM=";
	rev = "73823e5d88c94d59283701b23487e9a87a411b1e";
	src = fetchFromGitLab {
		inherit rev;
		owner = "pawelstrzebonski";
		repo = "rusume";
		sha256 = "sha256-pvh52xeC+irZuvD8OXwDRs/QS+y8+RF/yv7o04vxb5k=";
	};
	meta = with lib; {
		description = "A Rust command line application for generating resume documents from a JSON Resume file.";
		homepage = "https://gitlab.com/pawelstrzebonski/rusume";
		license = licenses.agpl3;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
